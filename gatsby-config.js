require(`dotenv`).config({
  path: `.env.${process.env.NODE_ENV}`
});

module.exports = {
  siteMetadata: {
    title: `Text Tool`,
    description: `A simple tool for working with website text.`,
    author: `@bobhumphrey`,
    siteUrl: `https://fonts.bob-humphrey.com`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-postcss`,
    {
      resolve: `gatsby-plugin-purgecss`,
      options: {
        tailwind: true,
        purgeOnly: [`src/css/style.css`],
        whitelistPatterns: [/^bg-/, /^text-/]
      }
    },
    `gatsby-plugin-offline`,
    {
      resolve: `gatsby-plugin-robots-txt`,
      options: {
        host: `https://XXXXXXXXXX.bob-humphrey.com`,
        env: {
          development: {
            policy: [{ userAgent: `*`, disallow: [`/`] }]
          },
          production: {
            policy: [{ userAgent: `*`, disallow: [`/`] }]
          }
        }
      }
    }
  ]
};
