// See https://tailwindcss.com/docs/configuration for details

module.exports = {
  theme: {
    extend: {
      spacing: {
        "72": `18rem`
      }
    },

    fontFamily: {
      sans: [
        `system-ui`,
        `-apple-system`,
        `BlinkMacSystemFont`,
        `"Segoe UI"`,
        `Roboto`,
        `"Helvetica Neue"`,
        `Arial`,
        `"Noto Sans"`,
        `sans-serif`,
        `"Apple Color Emoji"`,
        `"Segoe UI Emoji"`,
        `"Segoe UI Symbol"`,
        `"Noto Color Emoji"`
      ],
      serif: [
        `roboto_slabregular`,
        `Georgia`,
        `Cambria`,
        `"Times New Roman"`,
        `Times`,
        `serif`
      ],
      mono: [
        `Menlo`,
        `Monaco`,
        `Consolas`,
        `"Liberation Mono"`,
        `"Courier New"`,
        `monospace`
      ],
      chunk: [`chunk`],
      adelle_bold: [`adelle_bold`],
      adelle_bold_italic: [`adelle_bold_italic`],
      adelle_extra_bold: [`adelle_extra_bold`],
      adelle_extra_bold_italic: [`adelle_extra_bold_italic`],
      adelle_heavy: [`adelle_heavy`],
      adelle_heavy_italic: [`adelle_heavy_italic`],
      adelle_italic: [`adelle_italic`],
      adelle_light: [`adelle_light`],
      adelle_light_italic: [`adelle_light_italic`],
      adelle_regular: [`adelle_regular`],
      adelle_semi_bold: [`adelle_semi_bold`],
      adelle_semi_bold_italic: [`adelle_semi_bold_italic`],
      inter_black: [`inter_black`],
      inter_black_italic: [`inter_black_italic`],
      inter_bold: [`inter_bold`],
      inter_bold_italic: [`inter_bold_italic`],
      inter_extrabold: [`inter_extrabold`],
      inter_extrabold_italic: [`inter_extrabold_italic`],
      inter_extralight: [`inter_extralight`],
      inter_extralight_italic: [`inter_extralight_italic`],
      inter_italic: [`inter_italic`],
      inter_light: [`inter_light`],
      inter_light_italic: [`inter_light_italic`],
      inter_medium: [`inter_medium`],
      inter_medium_italic: [`inter_medium_italic`],
      inter_regular: [`inter_regular`],
      inter_semibold: [`inter_semibold`],
      inter_semibold_italic: [`inter_semibold_italic`],
      inter_thin: [`inter_thin`],
      inter_thin_italic: [`inter_thin_italic`],
      lato_black: [`lato_black`],
      lato_black_italic: [`lato_black_italic`],
      lato_bold: [`lato_bold`],
      lato_bold_italic: [`lato_bold_italic`],
      lato_hairline: [`lato_hairline`],
      lato_hairline_italic: [`lato_hairline_italic`],
      lato_heavy: [`lato_heavy`],
      lato_heavy_italic: [`lato_heavy_italic`],
      lato_light: [`lato_light`],
      lato_light_italic: [`lato_light_italic`],
      lato_medium: [`lato_medium`],
      lato_medium_italic: [`lato_medium_italic`],
      lato_semibold: [`lato_semibold`],
      lato_semibold_italic: [`lato_semibold_italic`],
      lato_thin: [`lato_thin`],
      lato_thin_italic: [`lato_thin_italic`],
      lato: [`lato_regular`],
      lato_italic: [`lato_italic`],
      roboto: [`roboto_slabregular`]
    }
  },
  variants: {},
  plugins: []
};
