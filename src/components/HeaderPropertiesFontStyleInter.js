import React from "react";
import HeaderPropertyButtonFont from "./HeaderPropertyButtonFont";

const HeaderPropertiesFontStyleInter = () => {
  return (
    <div className="grid grid-cols-3">
      <div className="flex flex-col">
        <HeaderPropertyButtonFont
          property="font-inter_thin"
          display="Inter Thin"
        />
        <HeaderPropertyButtonFont
          property="font-inter_thin_italic"
          display="Inter Thin Italic"
        />
        <HeaderPropertyButtonFont
          property="font-inter_extralight"
          display="Inter Extra Light"
        />
        <HeaderPropertyButtonFont
          property="font-inter_extralight_italic"
          display="Inter Extra Light Italic"
        />
        <HeaderPropertyButtonFont
          property="font-inter_light"
          display="Inter Light"
        />
        <HeaderPropertyButtonFont
          property="font-inter_light_italic"
          display="Inter Light Italic"
        />
      </div>
      <div className="flex flex-col">
        <HeaderPropertyButtonFont
          property="font-inter_regular"
          display="Inter Regular"
        />
        <HeaderPropertyButtonFont
          property="font-inter_italic"
          display="Inter Italic"
        />
        <HeaderPropertyButtonFont
          property="font-inter_medium"
          display="Inter Medium"
        />
        <HeaderPropertyButtonFont
          property="font-inter_medium_italic"
          display="Inter Medium Italic"
        />
        <HeaderPropertyButtonFont
          property="font-inter_semibold"
          display="Inter Semi Bold"
        />
        <HeaderPropertyButtonFont
          property="font-inter_semibold_italic"
          display="Inter Semi Bold Italic"
        />
      </div>
      <div className="flex flex-col">
        <HeaderPropertyButtonFont
          property="font-inter_bold"
          display="Inter Bold"
        />
        <HeaderPropertyButtonFont
          property="font-inter_bold_italic"
          display="Inter Bold Italic"
        />
        <HeaderPropertyButtonFont
          property="font-inter_extrabold"
          display="Inter Extra Bold"
        />
        <HeaderPropertyButtonFont
          property="font-inter_extrabold_italic"
          display="Inter Extra Bold Italic"
        />
        <HeaderPropertyButtonFont
          property="font-inter_black"
          display="Inter Black"
        />
        <HeaderPropertyButtonFont
          property="font-inter_black_italic"
          display="Inter Black Italic"
        />
      </div>
    </div>
  );
};

export default HeaderPropertiesFontStyleInter;
