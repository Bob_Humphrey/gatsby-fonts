import React from "react";
import HeaderPropertyButtonFont from "./HeaderPropertyButtonFont";

const HeaderPropertiesFontStyleLato = () => {
  return (
    <div className="grid grid-cols-3">
      <div className="flex flex-col">
        <HeaderPropertyButtonFont
          property="font-lato_hairline"
          display="Lato Hairline"
        />
        <HeaderPropertyButtonFont
          property="font-lato_hairline_italic"
          display="Lato Hairline Italic"
        />
        <HeaderPropertyButtonFont
          property="font-lato_thin"
          display="Lato Thin"
        />
        <HeaderPropertyButtonFont
          property="font-lato_thin_italic"
          display="Lato Thin Italic"
        />
        <HeaderPropertyButtonFont
          property="font-lato_light"
          display="Lato Light"
        />
        <HeaderPropertyButtonFont
          property="font-lato_light_italic"
          display="Lato Light Italic"
        />
      </div>
      <div className="flex flex-col">
        <HeaderPropertyButtonFont property="font-lato" display="Lato Regular" />
        <HeaderPropertyButtonFont
          property="font-lato_italic"
          display="Lato Italic"
        />
        <HeaderPropertyButtonFont
          property="font-lato_medium"
          display="Lato Medium"
        />
        <HeaderPropertyButtonFont
          property="font-lato_medium_italic"
          display="Lato Medium Italic"
        />
        <HeaderPropertyButtonFont
          property="font-lato_semibold"
          display="Lato Semi Bold"
        />
        <HeaderPropertyButtonFont
          property="font-lato_semibold_italic"
          display="Lato Semi Bold Italic"
        />
      </div>
      <div className="flex flex-col">
        <HeaderPropertyButtonFont
          property="font-lato_bold"
          display="Lato Bold"
        />
        <HeaderPropertyButtonFont
          property="font-lato_bold_italic"
          display="Lato Bold Italic"
        />
        <HeaderPropertyButtonFont
          property="font-lato_heavy"
          display="Lato Heavy"
        />
        <HeaderPropertyButtonFont
          property="font-lato_heavy_italic"
          display="Lato Heavy Italic"
        />
        <HeaderPropertyButtonFont
          property="font-lato_black"
          display="Lato Black"
        />
        <HeaderPropertyButtonFont
          property="font-lato_black_italic"
          display="Lato Black Italic"
        />
      </div>
    </div>
  );
};

export default HeaderPropertiesFontStyleLato;
