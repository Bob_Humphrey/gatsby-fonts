import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";

const HeaderClasses = () => {
  const state = useContext(GlobalStateContext);

  return (
    <div className="grid grid-cols-1">
      <div className="flex flex-col">
        <div className="">
          BACKGROUND{":  "}
          <span className="font-mono">
            {state.backgroundWidth + " " + state.backgroundColor}
          </span>
        </div>
        <div className="">
          HEADING{":  "}
          <span className="font-mono">
            {state.headingFont +
              " " +
              state.headingSize +
              " " +
              state.headingLineHeight +
              " " +
              state.headingColor}
          </span>
        </div>
        <div className="">
          BODY{":  "}
          <span className="font-mono">
            {state.bodyFont +
              " " +
              state.bodySize +
              " " +
              state.bodyLineHeight +
              " " +
              state.bodyColor}
          </span>
        </div>
      </div>
      <div className="flex flex-col"></div>
      <div className=""></div>
    </div>
  );
};

export default HeaderClasses;
