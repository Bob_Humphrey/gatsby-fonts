import React from "react";
import HeaderPropertyButtonFont from "./HeaderPropertyButtonFont";

const HeaderPropertiesFontStyleInter = () => {
  return (
    <div className="grid grid-cols-3">
      <div className="flex flex-col">
        <HeaderPropertyButtonFont
          property="font-adelle_light"
          display="Adelle Light"
        />
        <HeaderPropertyButtonFont
          property="font-adelle_light_italic"
          display="Adelle Light Italic"
        />
        <HeaderPropertyButtonFont
          property="font-adelle_regular"
          display="Adelle Regular"
        />
        <HeaderPropertyButtonFont
          property="font-adelle_italic"
          display="Adelle Italic"
        />
        <HeaderPropertyButtonFont
          property="font-adelle_semi_bold"
          display="Adelle SemiBold"
        />
        <HeaderPropertyButtonFont
          property="font-adelle_semi_bold_italic"
          display="Adelle SemiBold Italic"
        />
      </div>
      <div className="flex flex-col">
        <HeaderPropertyButtonFont
          property="font-adelle_bold"
          display="Adelle Bold"
        />
        <HeaderPropertyButtonFont
          property="font-adelle_bold_italic"
          display="Adelle Bold Italic"
        />
        <HeaderPropertyButtonFont
          property="font-adelle_extra_bold"
          display="Adelle ExtraBold"
        />
        <HeaderPropertyButtonFont
          property="font-adelle_extra_bold_italic"
          display="Adelle ExtraBold Italic"
        />
        <HeaderPropertyButtonFont
          property="font-adelle_heavy"
          display="Adelle Heavy"
        />
        <HeaderPropertyButtonFont
          property="font-adelle_heavy_italic"
          display="Adelle Heavy Italic"
        />
      </div>
      <div className="flex flex-col"></div>
    </div>
  );
};

export default HeaderPropertiesFontStyleInter;
