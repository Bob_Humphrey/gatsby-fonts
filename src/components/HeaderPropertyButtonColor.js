import React, { useContext } from "react";
import {
  GlobalStateContext,
  GlobalDispatchContext
} from "../context/GlobalContextProvider";

const HeaderPropertyButtonColor = props => {
  const state = useContext(GlobalStateContext);
  const dispatch = useContext(GlobalDispatchContext);
  const displayProperty = props.property.replace("text", "bg");
  let type, color;
  if (state.category === "Background" && state.subCategory === "Color") {
    type = "CHANGE_BACKGROUND_COLOR";
    color =
      state.backgroundColor === props.property
        ? "border-l-8 border-r-4 border-red-600"
        : "";
  }
  if (state.category === "Heading" && state.subCategory === "Color") {
    type = "CHANGE_HEADING_COLOR";
    color =
      state.headingColor === props.property
        ? "border-l-8 border-r-4 border-red-600"
        : "";
  }
  if (state.category === "Body" && state.subCategory === "Color") {
    type = "CHANGE_BODY_COLOR";
    color =
      state.bodyColor === props.property
        ? "border-l-8 border-r-4 border-red-600"
        : "";
  }
  return (
    <button
      className="cursor-pointer focus:outline-none"
      onClick={() => {
        dispatch({
          type: type,
          property: props.property
        });
      }}
    >
      <div
        className={
          color +
          " " +
          displayProperty +
          " self-center h-3 w-2 md:w-3 rounded-sm mr-3"
        }
      >
        &nbsp;
      </div>
    </button>
  );
};

export default HeaderPropertyButtonColor;
