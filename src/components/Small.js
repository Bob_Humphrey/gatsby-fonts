import React from "react";

const Small = () => {
  return (
    <main className="flex justify-center py-24 px-6">
      <div className="text-xl font-inter">
        A wider screen display is required to use this application.
      </div>
    </main>
  );
};

export default Small;
