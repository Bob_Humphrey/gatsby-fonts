import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";

const HeaderPropertiesFontStyleNone = () => {
  const state = useContext(GlobalStateContext);
  const font =
    state.category === "Heading"
      ? state.headingFontFamily
      : state.bodyFontFamily;

  return <div className="">No styles for the {font} font</div>;
};

export default HeaderPropertiesFontStyleNone;
