import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import HeaderPropertyButtonColor from "./HeaderPropertyButtonColor";

const HeaderPropertiesColorRow = props => {
  const state = useContext(GlobalStateContext);

  const tailwindValues = [
    "100",
    "200",
    "300",
    "400",
    "500",
    "600",
    "700",
    "800",
    "900"
  ];

  const colorType = state.category === "Background" ? "bg-" : "text-";

  return (
    <div className="">
      {tailwindValues.map(function(value, index) {
        return (
          <HeaderPropertyButtonColor
            property={colorType + props.hue + "-" + value}
            key={index}
          />
        );
      })}
    </div>
  );
};

export default HeaderPropertiesColorRow;
