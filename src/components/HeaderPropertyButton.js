import React, { useContext } from "react";
import {
  GlobalStateContext,
  GlobalDispatchContext
} from "../context/GlobalContextProvider";

const HeaderPropertyButton = props => {
  const state = useContext(GlobalStateContext);
  const dispatch = useContext(GlobalDispatchContext);
  let type, color;
  if (state.category === "Background" && state.subCategory === "Width") {
    type = "CHANGE_BACKGROUND_WIDTH";
    color = state.backgroundWidth === props.property ? "text-red-600" : "";
  }
  if (state.category === "Background" && state.subCategory === "Color") {
    type = "CHANGE_BACKGROUND_COLOR";
    color = state.backgroundColor === props.property ? "text-red-600" : "";
  }
  if (state.category === "Heading" && state.subCategory === "Size") {
    type = "CHANGE_HEADING_SIZE";
    color = state.headingSize === props.property ? "text-red-600" : "";
  }
  if (state.category === "Heading" && state.subCategory === "Line Height") {
    type = "CHANGE_HEADING_LINEHEIGHT";
    color = state.headingLineHeight === props.property ? "text-red-600" : "";
  }
  if (state.category === "Heading" && state.subCategory === "Color") {
    type = "CHANGE_HEADING_COLOR";
    color = state.headingColor === props.property ? "text-red-600" : "";
  }
  if (state.category === "Body" && state.subCategory === "Size") {
    type = "CHANGE_BODY_SIZE";
    color = state.bodySize === props.property ? "text-red-600" : "";
  }
  if (state.category === "Body" && state.subCategory === "Line Height") {
    type = "CHANGE_BODY_LINEHEIGHT";
    color = state.bodyLineHeight === props.property ? "text-red-600" : "";
  }
  if (state.category === "Body" && state.subCategory === "Color") {
    type = "CHANGE_BODY_COLOR";
    color = state.bodyColor === props.property ? "text-red-600" : "";
  }
  return (
    <button
      className="cursor-pointer focus:outline-none"
      onClick={() => {
        dispatch({
          type: type,
          property: props.property
        });
      }}
    >
      <div
        className={color + " " + " text-left hover:text-red-600 leading-tight"}
      >
        {props.property}
      </div>
    </button>
  );
};

export default HeaderPropertyButton;
