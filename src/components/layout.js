import PropTypes from "prop-types";
import React from "react";
import Header from "./header";
import SEO from "./seo";
import Small from "./Small";
import Footer from "./Footer";

function Layout({ children }) {
  return (
    <div className="">
      <Header />
      <SEO keywords={[`fonts`, `web design`]} title="Font Tool" />
      <div className="sm:hidden">
        <Small />
      </div>
      <main className="hidden sm:block">{children}</main>

      <Footer />
    </div>
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired
};

export default Layout;
