import React from "react";
import HeaderPropertiesWidth from "./HeaderPropertiesWidth";
import HeaderPropertiesColor from "./HeaderPropertiesColor";
import HeaderPropertiesFont from "./HeaderPropertiesFont";
import HeaderPropertiesFontStyle from "./HeaderPropertiesFontStyle";
import HeaderPropertiesSize from "./HeaderPropertiesSize";
import HeaderPropertiesLineHeight from "./HeaderPropertiesLineHeight";
import HeaderPages from "./HeaderPages";
import HeaderClasses from "./HeaderClasses";

const HeaderProperties = props => {
  const showWidth = props.subCategory === "Width" ? "" : "hidden";
  const showColor = props.subCategory === "Color" ? "" : "hidden";
  const showFont = props.subCategory === "Font" ? "" : "hidden";
  const showFontStyle = props.subCategory === "Font Style" ? "" : "hidden";
  const showSize = props.subCategory === "Size" ? "" : "hidden";
  const showLineHeight = props.subCategory === "Line Height" ? "" : "hidden";
  const showPages = props.category === "Pages" ? " " : "hidden";
  const showClasses = props.category === "Classes" ? "" : "hidden";

  return (
    <div>
      <div className={showWidth}>
        <HeaderPropertiesWidth
          category={props.category}
          subCategory={props.subCategory}
        />
      </div>
      <div className={showColor}>
        <HeaderPropertiesColor
          category={props.category}
          subCategory={props.subCategory}
        />
      </div>
      <div className={showFont}>
        <HeaderPropertiesFont
          category={props.category}
          subCategory={props.subCategory}
        />
      </div>
      <div className={showFontStyle}>
        <HeaderPropertiesFontStyle
          category={props.category}
          subCategory={props.subCategory}
        />
      </div>
      <div className={showSize}>
        <HeaderPropertiesSize
          category={props.category}
          subCategory={props.subCategory}
        />
      </div>
      <div className={showLineHeight}>
        <HeaderPropertiesLineHeight
          category={props.category}
          subCategory={props.subCategory}
        />
      </div>
      <div className={showPages}>
        <HeaderPages category={props.category} subCategory="None" />
      </div>
      <div className={showClasses}>
        <HeaderClasses category={props.category} subCategory="None" />
      </div>
    </div>
  );
};

export default HeaderProperties;
