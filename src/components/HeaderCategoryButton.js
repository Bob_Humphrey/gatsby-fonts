import React, { useContext } from "react";
import {
  GlobalStateContext,
  GlobalDispatchContext
} from "../context/GlobalContextProvider";

const HeaderCategoryButton = props => {
  const state = useContext(GlobalStateContext);
  const dispatch = useContext(GlobalDispatchContext);
  let type, color;
  let show = "";
  switch (props.type) {
    case "category":
      type = `CHANGE_CATEGORY`;
      color = props.property === state.category ? "text-red-600" : "";
      break;
    case "subCategory":
      type = `CHANGE_SUBCATEGORY`;

      show = props.category === state.category ? "" : "hidden";
      switch (props.category) {
        case "Background":
          color =
            props.property === state.backgroundSubCategory
              ? "text-red-600"
              : "";
          break;
        case "Heading":
          color =
            props.property === state.headingSubCategory ? "text-red-600" : "";
          break;
        case "Body":
          color =
            props.property === state.bodySubCategory ? "text-red-600" : "";
          break;
        default:
          break;
      }
      break;
    default:
      break;
  }
  return (
    <button
      className="cursor-pointer focus:outline-none"
      onClick={() => {
        dispatch({
          type: type,
          property: props.property
        });
      }}
    >
      <div
        className={
          color + " " + show + " text-left hover:text-red-600 leading-tight"
        }
      >
        {props.property}
      </div>
    </button>
  );
};

export default HeaderCategoryButton;
