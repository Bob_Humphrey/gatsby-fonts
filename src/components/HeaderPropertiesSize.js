import React from "react";
import HeaderPropertyButton from "./HeaderPropertyButton";

const HeaderPropertiesSize = () => {
  return (
    <div className="grid grid-cols-3">
      <div className="flex flex-col">
        <HeaderPropertyButton property="text-xs" />
        <HeaderPropertyButton property="text-sm" />
        <HeaderPropertyButton property="text-base" />
        <HeaderPropertyButton property="text-lg" />
        <HeaderPropertyButton property="text-xl" />
        <HeaderPropertyButton property="text-2xl" />
      </div>
      <div className="flex flex-col">
        <HeaderPropertyButton property="text-3xl" />
        <HeaderPropertyButton property="text-4xl" />
        <HeaderPropertyButton property="text-5xl" />
        <HeaderPropertyButton property="text-6xl" />
      </div>
      <div className=""></div>
    </div>
  );
};

export default HeaderPropertiesSize;
