import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import HeaderPropertiesFontStyleNone from "./HeaderPropertiesFontStyleNone";
import HeaderPropertiesFontStyleAdelle from "./HeaderPropertiesFontStyleAdelle";
import HeaderPropertiesFontStyleInter from "./HeaderPropertiesFontStyleInter";
import HeaderPropertiesFontStyleLato from "./HeaderPropertiesFontStyleLato";

const HeaderPropertiesFontStyle = () => {
  const state = useContext(GlobalStateContext);
  let showInter = "hidden";
  let showLato = "hidden";
  let showAdelle = "hidden";
  let showNone = "";
  const currentFontFamily =
    state.category === "Heading"
      ? state.headingFontFamily
      : state.bodyFontFamily;
  switch (currentFontFamily) {
    case "Adelle":
      showAdelle = "";
      showNone = "hidden";
      break;
    case "Inter":
      showInter = "";
      showNone = "hidden";
      break;
    case "Lato":
      showLato = "";
      showNone = "hidden";
      break;
    default:
      break;
  }

  return (
    <div>
      <div className={showNone}>
        <HeaderPropertiesFontStyleNone />
      </div>
      <div className={showAdelle}>
        <HeaderPropertiesFontStyleAdelle />
      </div>
      <div className={showInter}>
        <HeaderPropertiesFontStyleInter />
      </div>
      <div className={showLato}>
        <HeaderPropertiesFontStyleLato />
      </div>
    </div>
  );
};

export default HeaderPropertiesFontStyle;
