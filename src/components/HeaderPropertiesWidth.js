import React from "react";
import HeaderPropertyButton from "./HeaderPropertyButton";

const HeaderPropertiesWidth = () => {
  return (
    <div className="grid grid-cols-3">
      <div className="flex flex-col">
        <HeaderPropertyButton property="w-full" />
        <HeaderPropertyButton property="w-11/12" />
        <HeaderPropertyButton property="w-5/6" />
        <HeaderPropertyButton property="w-3/4" />
        <HeaderPropertyButton property="w-2/3" />
        <HeaderPropertyButton property="w-7/12" />
      </div>
      <div className="flex flex-col">
        <HeaderPropertyButton property="w-1/2" />
        <HeaderPropertyButton property="w-5/12" />
        <HeaderPropertyButton property="w-1/3" />
        <HeaderPropertyButton property="w-1/4" />
        <HeaderPropertyButton property="w-1/6" />
        <HeaderPropertyButton property="w-1/12" />
      </div>
      <div className=""></div>
    </div>
  );
};

export default HeaderPropertiesWidth;
