import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import HeaderCategoryButton from "./HeaderCategoryButton";
import HeaderProperties from "./HeaderProperties";

const Header = () => {
  const state = useContext(GlobalStateContext);

  return (
    <header className="hidden sm:block bg-gray-100 text-sm pt-4 pb-2">
      <div className="container w-full lg:w-3/4 xl:w-3/5 mx-auto">
        <div className="grid grid-cols-8">
          <div className="col-span-1 flex flex-col pb-8">
            <HeaderCategoryButton type="category" property="Background" />
            <HeaderCategoryButton type="category" property="Heading" />
            <HeaderCategoryButton type="category" property="Body" />
            <HeaderCategoryButton type="category" property="Pages" />
            <HeaderCategoryButton type="category" property="Classes" />
          </div>
          <div className="col-span-1 flex flex-col">
            <HeaderCategoryButton
              type="subCategory"
              category="Background"
              property="Color"
            />
            <HeaderCategoryButton
              type="subCategory"
              category="Background"
              property="Width"
            />
            <HeaderCategoryButton
              type="subCategory"
              category="Heading"
              property="Font"
            />
            <HeaderCategoryButton
              type="subCategory"
              category="Heading"
              property="Font Style"
            />
            <HeaderCategoryButton
              type="subCategory"
              category="Heading"
              property="Size"
            />
            <HeaderCategoryButton
              type="subCategory"
              category="Heading"
              property="Line Height"
            />
            <HeaderCategoryButton
              type="subCategory"
              category="Heading"
              property="Color"
            />
            <HeaderCategoryButton
              type="subCategory"
              category="Body"
              property="Font"
            />
            <HeaderCategoryButton
              type="subCategory"
              category="Body"
              property="Font Style"
            />
            <HeaderCategoryButton
              type="subCategory"
              category="Body"
              property="Size"
            />
            <HeaderCategoryButton
              type="subCategory"
              category="Body"
              property="Line Height"
            />
            <HeaderCategoryButton
              type="subCategory"
              category="Body"
              property="Color"
            />
          </div>
          <div className="col-span-6">
            <HeaderProperties
              category={state.category}
              subCategory={state.subCategory}
            />
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
