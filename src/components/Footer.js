import React from "react";

const Footer = () => {
  return (
    <footer className="flex justify-center py-16 mt-4">
      <div className="w-16">
        <a href="https://bob-humphrey.com">
          <img
            alt="Bob Humphrey website"
            src="https://images.bob-humphrey.com/bh-logo.gif"
          />
        </a>
      </div>
    </footer>
  );
};

export default Footer;
