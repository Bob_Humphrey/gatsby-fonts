import React from "react";
import HeaderPropertyButtonFont from "./HeaderPropertyButtonFont";

const HeaderPropertiesFont = () => {
  return (
    <div className="grid grid-cols-3">
      <div className="flex flex-col">
        <HeaderPropertyButtonFont property="font-sans" display="Sans Serif" />
        <HeaderPropertyButtonFont property="font-lato" display="Lato" />
        <HeaderPropertyButtonFont
          property="font-inter_regular"
          display="Inter"
        />
        <HeaderPropertyButtonFont
          property="font-adelle_regular"
          display="Adelle"
        />
        <HeaderPropertyButtonFont property="font-chunk" display="Chunk" />
        <HeaderPropertyButtonFont property="font-roboto" display="Roboto" />
      </div>
      <div className="flex flex-col">
        <HeaderPropertyButtonFont property="font-mono" display="Mono" />
      </div>
      <div className=""></div>
    </div>
  );
};

export default HeaderPropertiesFont;
