import React from "react";
import { Link } from "gatsby";

const HeaderPages = () => {
  return (
    <div className="grid grid-cols-2 leading-tight">
      <div className="flex flex-col">
        <Link to={`/`}>Home</Link>
        <Link to={`/mccullers/`}>The Heart Is a Lonely Hunter</Link>
        <Link to={`/capote/`}>Breakfast at Tiffany's</Link>
        <Link to={`/franklin/`}>Autobiography of Benjamin Franklin</Link>
        <Link to={`/wright/`}>Black Boy</Link>
        <Link to={`/shakespeare/`}>Julius Caesar</Link>
      </div>
      <div className="flex flex-col">
        <Link to={`/frost/`}>Acquainted with the Night</Link>
        <Link to={`/steinbeck/`}>The Winter of Our Discontent</Link>
        <Link to={`/lee/`}>To Kill a Mockingbird</Link>
        <Link to={`/faulkner/`}>As I Lay Dying</Link>
        <Link to={`/oliver/`}>Angels</Link>
      </div>
    </div>
  );
};

export default HeaderPages;
