import React from "react";
import HeaderPropertyButton from "./HeaderPropertyButton";

const HeaderPropertiesLineHeight = () => {
  return (
    <div className="grid grid-cols-3">
      <div className="flex flex-col">
        <HeaderPropertyButton property="leading-none" />
        <HeaderPropertyButton property="leading-tight" />
        <HeaderPropertyButton property="leading-snug" />
        <HeaderPropertyButton property="leading-normal" />
        <HeaderPropertyButton property="leading-relaxed" />
        <HeaderPropertyButton property="leading-loose" />
      </div>
      <div className="flex flex-col">
        <HeaderPropertyButton property="leading-3" />
        <HeaderPropertyButton property="leading-4" />
        <HeaderPropertyButton property="leading-5" />
        <HeaderPropertyButton property="leading-6" />
        <HeaderPropertyButton property="leading-7" />
        <HeaderPropertyButton property="leading-8" />
      </div>
      <div className="flex flex-col">
        <HeaderPropertyButton property="leading-9" />
        <HeaderPropertyButton property="leading-10" />
      </div>
    </div>
  );
};

export default HeaderPropertiesLineHeight;
