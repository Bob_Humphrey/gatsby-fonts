import React, { useContext } from "react";
import { GlobalStateContext } from "../context/GlobalContextProvider";
import HeaderPropertyButton from "./HeaderPropertyButton";
import HeaderPropertiesColorRow from "./HeaderPropertiesColorRow";

const HeaderPropertiesColor = () => {
  const state = useContext(GlobalStateContext);
  const tailwindHues = [
    "gray",
    "red",
    "orange",
    "yellow",
    "green",
    "teal",
    "blue",
    "indigo",
    "purple",
    "pink"
  ];
  const blackProperty =
    state.category === "Background" ? "bg-black" : "text-black";
  const whiteProperty =
    state.category === "Background" ? "bg-white" : "text-white";

  return (
    <div className="grid grid-cols-5">
      <div className="col-span-2 flex flex-col">
        {tailwindHues.map(function(hue, index) {
          if (index > 4) return null;
          return <HeaderPropertiesColorRow hue={hue} key={index} />;
        })}
      </div>
      <div className="col-span-2 flex flex-col">
        {tailwindHues.map(function(hue, index) {
          if (index < 5) return null;
          return <HeaderPropertiesColorRow hue={hue} key={index} />;
        })}
      </div>
      <div className="flex flex-col">
        <HeaderPropertyButton property={blackProperty} />
        <HeaderPropertyButton property={whiteProperty} />
      </div>
    </div>
  );
};

export default HeaderPropertiesColor;
