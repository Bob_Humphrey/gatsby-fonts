import React, { useContext } from "react";
import {
  GlobalStateContext,
  GlobalDispatchContext
} from "../context/GlobalContextProvider";

const HeaderPropertyButtonFont = props => {
  const state = useContext(GlobalStateContext);
  const dispatch = useContext(GlobalDispatchContext);
  let type, color;
  if (state.category === "Heading") {
    type = "CHANGE_HEADING_FONT";
    if (state.subCategory === "Font") {
      color = state.headingFontFamily === props.display ? "text-red-600" : "";
    } else {
      color = state.headingFont === props.property ? "text-red-600" : "";
    }
  }
  if (state.category === "Body") {
    type = "CHANGE_BODY_FONT";
    if (state.subCategory === "Font") {
      color = state.bodyFontFamily === props.display ? "text-red-600" : "";
    } else {
      color = state.bodyFont === props.property ? "text-red-600" : "";
    }
  }

  return (
    <button
      className="cursor-pointer focus:outline-none"
      onClick={() => {
        dispatch({
          type: type,
          property: props.property,
          family: props.display
        });
      }}
    >
      <div className={color + " text-left hover:text-red-600 leading-tight"}>
        {props.display}
      </div>
    </button>
  );
};

export default HeaderPropertyButtonFont;
