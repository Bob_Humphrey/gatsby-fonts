import React, { useContext } from "react";
import Layout from "../components/layout";
import { GlobalStateContext } from "../context/GlobalContextProvider";

function SteinbeckPage() {
  const state = useContext(GlobalStateContext);

  return (
    <Layout>
      <div className={state.backgroundColor + " flex justify-center"}>
        <div
          className={state.backgroundWidth + "  mx-auto text-justify py-12 "}
        >
          <div
            className={
              state.headingFont +
              " " +
              state.headingSize +
              " " +
              state.headingLineHeight +
              " " +
              state.headingColor +
              " mb-6"
            }
          >
            To Kill a Mockingbird
          </div>
          <div
            className={
              state.bodyFont +
              " " +
              state.bodySize +
              " " +
              state.bodyLineHeight +
              " " +
              state.bodyColor +
              "  mx-auto text-justify py-6 "
            }
          >
            <p className="">
              Maycomb was an old town, but it was a tired old town when I first
              knew it. In rainy weather the streets turned to red slop; grass
              grew on the sidewalks, the courthouse sagged in the square.
              Somehow, it was hotter then: a black dog suffered on a summer’s
              day; bony mules hitched to Hoover carts flicked flies in the
              sweltering shade of the live oaks on the square. Men’s stiff
              collars wilted by nine in the morning. Ladies bathed before noon,
              after their three-o’clock naps, and by nightfall were like soft
              teacakes with frostings of sweat and sweet talcum. People moved
              slowly then. They ambled across the square, shuffled in and out of
              the stores around it, took their time about everything. A day was
              twenty-four hours long but seemed longer. There was no hurry, for
              there was nowhere to go, nothing to buy and no money to buy it
              with, nothing to see outside the boundaries of Maycomb County. But
              it was a time of vague optimism for some of the people: Maycomb
              County had recently been told that it had nothing to fear but fear
              itself.
            </p>
            <p className="">
              We lived on the main residential street in town— Atticus, Jem and
              I, plus Calpurnia our cook. Jem and I found our father
              satisfactory: he played with us, read to us, and treated us with
              courteous detachment.
            </p>
            <p className="">
              Calpurnia was something else again. She was all angles and bones;
              she was nearsighted; she squinted; her hand was wide as a bed slat
              and twice as hard. She was always ordering me out of the kitchen,
              asking me why I couldn’t behave as well as Jem when she knew he
              was older, and calling me home when I wasn’t ready to come. Our
              battles were epic and one-sided. Calpurnia always won, mainly
              because Atticus always took her side. She had been with us ever
              since Jem was born, and I had felt her tyrannical presence as long
              as I could remember.
            </p>
            <p className="">
              Our mother died when I was two, so I never felt her absence. She
              was a Graham from Montgomery; Atticus met her when he was first
              elected to the state legislature. He was middle-aged then, she was
              fifteen years his junior. Jem was the product of their first year
              of marriage; four years later I was born, and two years later our
              mother died from a sudden heart attack. They said it ran in her
              family. I did not miss her, but I think Jem did. He remembered her
              clearly, and sometimes in the middle of a game he would sigh at
              length, then go off and play by himself behind the car-house. When
              he was like that, I knew better than to bother him.
            </p>
            <div className="mt-16">
              <a
                href="https://www.amazon.com/Kill-Mockingbird-Harper-Lee/dp/0060935464/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Harper Lee
              </a>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default SteinbeckPage;
