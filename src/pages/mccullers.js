import React, { useContext } from "react";
import Layout from "../components/layout";
import { GlobalStateContext } from "../context/GlobalContextProvider";

function McCullersPage() {
  const state = useContext(GlobalStateContext);

  return (
    <Layout>
      <div className={state.backgroundColor + " flex justify-center"}>
        <div
          className={state.backgroundWidth + "  mx-auto text-justify py-12 "}
        >
          <div
            className={
              state.headingFont +
              " " +
              state.headingSize +
              " " +
              state.headingLineHeight +
              " " +
              state.headingColor +
              " mb-6"
            }
          >
            The Heart Is a Lonely Hunter
          </div>
          <div
            className={
              state.bodyFont +
              " " +
              state.bodySize +
              " " +
              state.bodyLineHeight +
              " " +
              state.bodyColor +
              "  mx-auto text-justify py-6 "
            }
          >
            <p className="">
              In the spring a change came over Singer. He could not sleep and
              his body was very restless. At evening he would walk monotonously
              around the room, unable to work off a new feeling of energy. If he
              rested at all it was only during a few hours before dawn -- then
              he would drop bluntly into a sleep that lasted until the morning
              light struck suddenly beneath his opening eyelids like a scimitar.
            </p>
            <p className="">
              He began spending his evenings walking around the town. He could
              no longer stand the rooms where Antonapoulos had lived, and he
              rented a place in a shambling boardinghouse not far from the
              center of the town.
            </p>
            <p className="">
              He ate his meals at a restaurant only two blocks away. This
              restaurant was at the end of the long main street and the name of
              the place was the New York Cafe. The first day he glanced over the
              menu quickly and wrote a short note and handed it to the
              proprietor.
            </p>
            <p className="">
              Each morning for breakfast I want an egg, toast, and coffee --
              $0.15.
            </p>
            <p className="">
              For lunch I want soup (any kind), a meat sandwich and milk --
              $0.25.
            </p>
            <p className="">
              Please bring me at dinner three vegetables (any kind but cabbage),
              fish or meat, and a glass of beer -- $0.35.
            </p>
            <p className="">Thank you.</p>
            <p className="">
              The proprietor read the note and gave him an alert, tactful
              glance. He was a hard man of middle height, with a beard so dark
              and heavy that the lower part of his face looked as though it were
              molded of iron. He usually stood in the corner by the cash
              register, his arms folded over his chest, quietly observing all
              that went on around him. Singer came to know this man's face very
              well, for he ate at one of his tables three times a day.
            </p>
            <p className="">
              Each evening the mute walked alone for hours in the street.
              Sometimes the nights were cold with the sharp, wet winds of March
              and it would be raining heavily. But to him this did not matter.
              His gait was agitated and he always kept his hands stuffed tight
              into the pockets of his trousers. Then as the weeks passed the
              days grew warm and languorous. His agitation gave way gradually to
              exhaustion and there was a look about him of deep calm. In his
              face there came to be a brooding peace that is seen most often in
              the faces of the very sorrowful or the very wise. But still he
              wandered throught the streets of the town, always silent and
              alone.
            </p>
            <div className="mt-16">
              <a
                href="https://www.amazon.com/Heart-Lonely-Hunter-Carson-McCullers-ebook/dp/B00Z9N882U/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Carson McCullers
              </a>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default McCullersPage;
