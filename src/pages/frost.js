import React, { useContext } from "react";
import Layout from "../components/layout";
import { GlobalStateContext } from "../context/GlobalContextProvider";

function FrostPage() {
  const state = useContext(GlobalStateContext);
  return (
    <Layout>
      <div className={state.backgroundColor + " flex justify-center"}>
        <div
          className={state.backgroundWidth + "  mx-auto text-justify py-12 "}
        >
          <div
            className={
              state.headingFont +
              " " +
              state.headingSize +
              " " +
              state.headingLineHeight +
              " " +
              state.headingColor +
              " mb-6"
            }
          >
            Acquainted with the Night
          </div>
          <div
            className={
              state.bodyFont +
              " " +
              state.bodySize +
              " " +
              state.bodyLineHeight +
              " " +
              state.bodyColor +
              "  mx-auto text-justify py-6 "
            }
          >
            <div className="">I have been one acquainted with the night.</div>
            <div className="">I have walked out in rain—and back in rain.</div>
            <div className="mb-8">
              I have outwalked the furthest city light.
            </div>
            <div className="">I have looked down the saddest city lane.</div>
            <div className="">I have passed by the watchman on his beat</div>
            <div className="mb-8">
              And dropped my eyes, unwilling to explain.
            </div>
            <div className="">
              I have stood still and stopped the sound of feet
            </div>
            <div className="">When far away an interrupted cry</div>
            <div className="mb-8">Came over houses from another street,</div>
            <div className="">But not to call me back or say good-bye;</div>
            <div className="">And further still at an unearthly height,</div>
            <div className="mb-8">One luminary clock against the sky</div>
            <div className="">
              Proclaimed the time was neither wrong nor right.{" "}
            </div>
            <div className="">I have been one acquainted with the night.</div>

            <div className="mt-16">
              <a
                href="https://www.amazon.com/Poetry-Robert-Frost-Collected-Unabridged/dp/0805005021/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Robert Frost
              </a>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default FrostPage;
