import React, { useContext } from "react";
import Layout from "../components/layout";
import { GlobalStateContext } from "../context/GlobalContextProvider";

function ShakespearePage() {
  const state = useContext(GlobalStateContext);

  return (
    <Layout>
      <div className={state.backgroundColor + " flex justify-center"}>
        <div
          className={state.backgroundWidth + "  mx-auto text-justify py-12 "}
        >
          <div
            className={
              state.headingFont +
              " " +
              state.headingSize +
              " " +
              state.headingLineHeight +
              " " +
              state.headingColor +
              " mb-6"
            }
          >
            Julius Caesar
          </div>
          <div
            className={
              state.bodyFont +
              " " +
              state.bodySize +
              " " +
              state.bodyLineHeight +
              " " +
              state.bodyColor +
              "  mx-auto text-justify py-6 "
            }
          >
            <p className="">SOOTHSAYER: Caesar!</p>
            <p className="">CAESAR: Ha! Who calls?</p>
            <p className="">
              CASCA: Bid every noise be still. Peace yet again!
            </p>
            <p className="">
              CAESAR: Who is it in the press that calls on me? I hear a tongue,
              shriller than all the music, Cry 'Caesar!' Speak. Caesar is turned
              to hear.
            </p>
            <p className="">SOOTHSAYER: Beware the Ides of March.</p>
            <p className="">CAESAR: What man is that?</p>
            <p className="">
              BRUTUS: A soothsayer bids you beware the ides of March.
            </p>
            <p className="">CAESAR: Set him before me. Let me see his face.</p>
            <p className="">
              CASSIUS: Fellow, come from the throng. Look upon Caesar.
            </p>
            <p className="">
              CAESAR: What sayst thou to me now? Speak once again.
            </p>
            <p className="">SOOTHSAYER: Beware the Ides of March.</p>
            <p className="">CAESAR: He is a dreamer. Let us leave him. Pass.</p>
            <div className="mt-16">
              <a
                href="https://www.amazon.com/Julius-Caesar-Folger-Shakespeare-Library/dp/0743482743/"
                target="_blank"
                rel="noopener noreferrer"
              >
                William Shakespeare
              </a>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default ShakespearePage;
