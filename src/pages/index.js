import React, { useContext } from "react";
import Layout from "../components/layout";
import { GlobalStateContext } from "../context/GlobalContextProvider";

function IndexPage() {
  const state = useContext(GlobalStateContext);

  return (
    <Layout>
      <div className={state.backgroundColor + " flex justify-center"}>
        <div
          className={state.backgroundWidth + "  mx-auto text-justify py-16 "}
        >
          <div
            className={
              state.headingFont +
              " " +
              state.headingSize +
              " " +
              state.headingLineHeight +
              " " +
              state.headingColor +
              " mb-6"
            }
          >
            <div className="text-center">A simple tool</div>
            <div className="text-center">for working with website text</div>
          </div>
          <div
            className={
              state.bodyFont +
              " " +
              state.bodySize +
              " " +
              state.bodyLineHeight +
              " " +
              state.bodyColor +
              "  mx-auto text-justify py-6 "
            }
          >
            <p>
              This tool can help you try out different options for formatting
              your website text.
            </p>
            <p>
              Use the menu bar above to change the properties of your
              background, heading, and text body.
            </p>
            <p>
              Click <b>Pages</b> to see your choices on different pages.
            </p>
            <p>
              Once you have the look you want, select <b>Classes</b> to see the{" "}
              <a
                className="underline"
                href="https://tailwindcss.com/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Tailwind CSS
              </a>{" "}
              classes used to format your page.
            </p>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default IndexPage;
