import React, { useContext } from "react";
import Layout from "../components/layout";
import { GlobalStateContext } from "../context/GlobalContextProvider";

function FaulknerPage() {
  const state = useContext(GlobalStateContext);

  return (
    <Layout>
      <div className={state.backgroundColor + " flex justify-center"}>
        <div
          className={state.backgroundWidth + "  mx-auto text-justify py-12 "}
        >
          <div
            className={
              state.headingFont +
              " " +
              state.headingSize +
              " " +
              state.headingLineHeight +
              " " +
              state.headingColor +
              " mb-6"
            }
          >
            As I Lay Dying
          </div>
          <div
            className={
              state.bodyFont +
              " " +
              state.bodySize +
              " " +
              state.bodyLineHeight +
              " " +
              state.bodyColor +
              "  mx-auto text-justify py-6 "
            }
          >
            <p className="">
              In the afternoon when school was out and the last one had left
              with his little dirty snuffling nose, instead of going home I
              would go down the hill to the spring where I could be quiet and
              hate them. It would be quiet there then, with the water bubbling
              up and away and the sun slanting quiet in the trees and the quiet
              smelling of damp and rotting leaves and new earth; especially in
              the early spring, for it was worst then.
            </p>
            <p className="">
              I could just remember how my father used to say that the reason
              for living was to get ready to stay dead a long time. And when I
              would have to look at them day after day, each with his and her
              secret and selfish thought, and blood strange to each other blood
              and strange to mine, and think that this seemed to be the only way
              I could get ready to stay dead, I would hate my father for having
              ever planted me. I would look forward to the times when they
              faulted, so I could whip them. When the switch fell I could feel
              it upon my flesh; when it welted and ridged it was my blood that
              ran, and I would think with each blow of the switch: Now you are
              aware of me! Now I am something in your secret and selfish life,
              who have marked your blood with my own for ever and ever.
            </p>
            <p className="">
              And so I took Anse. I saw him pass the school house three or four
              times before I learned that he was driving four miles out of his
              way to do it. I noticed then how he was beginning to hump--a tall
              man and young --so that he looked already like a tall bird hunched
              in the cold weather, on the wagon seat. He would pass the school
              house, the wagon creaking slow, his head turning slow to watch the
              door of the school house as the wagon passed, until he went on
              around the curve and out of sight. One day I went to the door and
              stood there when he passed. When he saw me he looked quickly away
              and did not look back again.
            </p>
            <p className="">
              In the early spring it was worst. Sometimes I thought that I could
              not bear it, lying in bed at night, with the wild geese going
              north and their honking coming faint and high and wild out of the
              wild darkness, and during the day it would seem as though I
              couldn't wait for the last one to go so I could go down to the
              spring. And so when I looked up that day and saw Anse standing
              there in his Sunday clothes, turning his hat round and round in
              his hands, I said:
            </p>
            <p className="">
              "If you've got any womenfolks, why in the world dont they make you
              get your hair cut?"
            </p>
            <p className="">
              "I aint got none," he said. Then he said suddenly, driving his
              eyes at me like two hounds in a strange yard: "That's what I come
              to see you about."
            </p>
            <p className="">
              "And make you hold your shoulders up," I said. "You haven't got
              any? But you've got a house. They tell me you've got a house and a
              good farm. And you live there alone, doing for yourself, do you?"
              He just looked at me, turning the hat in his hands. "A new house,"
              I said. "Are you going to get married?"
            </p>
            <p className="">
              And he said again, holding his eyes to mine: "That's what I come
              to see you about."
            </p>
            <p className="">
              Later he told me, "I aint got no people. So that wont be no worry
              to you. I dont reckon you can say the same."
            </p>

            <p className="">"No. I have people. In Jefferson."</p>
            <p className="">
              His face fell a little. "Well, I got a little property. I'm
              forehanded; I got a good honest name. I know how town folks are,
              but maybe when they talk to me . . ."
            </p>
            <p className="">
              "They might listen," I said. "But they'll be hard to talk to." He
              was watching my face. "They're in the cemetery."
            </p>
            <p className="">
              "But your living kin," he said. "They'll be different."
            </p>
            <p className="">
              "Will they?" I said. 'I dont know. I never had any other kind.”
            </p>
            <p className="">
              So I took Anse. And when I knew that I had Cash, I knew that
              living was terrible and that this was the answer to it. That was
              when I learned that words are no good; that words dont ever fit
              even what they are trying to say at. When he was born I knew that
              motherhood was invented by someone who had to have a word for it
              because the ones that had the children didn't care whether there
              was a word for it or not. I knew that fear was invented by someone
              that had never had the fear; pride, who never had the pride. I
              knew that it had been, not that they had dirty noses, but that we
              had had to use one another by words like spiders dangling by their
              mouths from a beam, swinging and twisting and never touching, and
              that only through the blows of the switch could my blood and their
              blood flow as one stream. I knew that it had been, not that my
              aloneness had to be violated over and over each day, but that it
              had never been violated until Cash came. Not even by Anse in the
              nights.
            </p>
            <div className="mt-16">
              <a
                href="https://www.amazon.com/As-Lay-Dying-Corrected-Text/dp/067973225X/"
                target="_blank"
                rel="noopener noreferrer"
              >
                William Faulkner
              </a>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default FaulknerPage;
