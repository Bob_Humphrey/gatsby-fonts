import React, { useContext } from "react";
import Layout from "../components/layout";
import { GlobalStateContext } from "../context/GlobalContextProvider";

function SteinbeckPage() {
  const state = useContext(GlobalStateContext);

  return (
    <Layout>
      <div className={state.backgroundColor + " flex justify-center"}>
        <div
          className={state.backgroundWidth + "  mx-auto text-justify py-12 "}
        >
          <div
            className={
              state.headingFont +
              " " +
              state.headingSize +
              " " +
              state.headingLineHeight +
              " " +
              state.headingColor +
              " mb-6"
            }
          >
            The Winter of Our Discontent
          </div>
          <div
            className={
              state.bodyFont +
              " " +
              state.bodySize +
              " " +
              state.bodyLineHeight +
              " " +
              state.bodyColor +
              "  mx-auto text-justify py-6 "
            }
          >
            <p className="">
              My wife, my Mary, goes to her sleep the way you would close the
              door of a closet. So many times I have watched her with envy. Her
              lovely body squirms a moment as though she fitted herself into a
              cocoon. She sighs once and at the end of it her eyes close and her
              lips, untroubled, fall into that wise and remote smile of the
              ancient Greek gods. She smiles all night in her sleep, her breath
              purrs in her throat, not a snore, a kitten's purr. For a moment
              her temperature leaps up so that I can feel the glow of it beside
              me in the bed, then drops and she has gone away. I don't know
              where. She says she does not dream. She must, of course. That
              simply means her dreams do not trouble her, or trouble her so much
              that she forgets them before awakening. She loves to sleep and
              sleep welcomes her. I wish it were so with me. I fight off sleep,
              at the same time craving it.
            </p>
            <p className="">
              I have thought the difference might be that my Mary knows she will
              live forever, that she will step from the living into another life
              as easily as she slips from sleep to wakefulness. She knows this
              with her whole body, so completely that she does not think of it
              any more than she thinks to breathe. Thus she has time to sleep,
              time to rest, time to cease to exist for a little.
            </p>
            <p className="">
              On the other hand, I know in my bones and my tissue that I will
              one day, soon or late, stop living and so I fight against sleep,
              and beseech it, even try to trick it into coming. My moment of
              sleep is a great wrench, an agony. I know this because I have
              awakened at this second still feeling the crushing blow. And once
              in sleep, I have a very busy time. My dreams are the problems of
              the day stepped up to absurdity, a little like men dancing,
              wearing the horns and masks of animals.
            </p>
            <div className="mt-16">
              <a
                href="https://www.amazon.com/Winter-Our-Discontent-Penguin-Classics/dp/0143039482/"
                target="_blank"
                rel="noopener noreferrer"
              >
                John Steinbeck
              </a>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default SteinbeckPage;
