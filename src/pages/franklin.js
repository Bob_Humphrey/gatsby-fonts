import React, { useContext } from "react";
import Layout from "../components/layout";
import { GlobalStateContext } from "../context/GlobalContextProvider";

function FranklinPage() {
  const state = useContext(GlobalStateContext);

  return (
    <Layout>
      <div className={state.backgroundColor + " flex justify-center"}>
        <div
          className={state.backgroundWidth + "  mx-auto text-justify py-12 "}
        >
          <div
            className={
              state.headingFont +
              " " +
              state.headingSize +
              " " +
              state.headingLineHeight +
              " " +
              state.headingColor +
              " mb-6"
            }
          >
            Autobiography
          </div>
          <div
            className={
              state.bodyFont +
              " " +
              state.bodySize +
              " " +
              state.bodyLineHeight +
              " " +
              state.bodyColor +
              "  mx-auto text-justify py-6 "
            }
          >
            <ol className="list-decimal list-inside">
              <li className="">
                Temperance. Eat not to dullness; drink not to elevation.
              </li>
              <li className="">
                Silence. Speak not but what may benefit others or yourself;
                avoid trifling conversation.
              </li>
              <li className="">
                Order. Let all your things have their places; let each part of
                your business have its time.
              </li>
              <li className="">
                Resolution. Resolve to perform what you ought; perform without
                fail what you resolve.
              </li>
              <li className="">
                Frugality. Make no expense but to do good to others or yourself;
                i.e., waste nothing.
              </li>
              <li className="">
                Industry. Lose no time; be always employ'd in something useful;
                cut off all unnecessary actions.
              </li>
              <li className="">
                Sincerity. Use no hurtful deceit; think innocently and justly,
                and, if you speak, speak accordingly.
              </li>
              <li className="">
                Justice. Wrong none by doing injuries, or omitting the benefits
                that are your duty.
              </li>
              <li className="">
                Moderation. Avoid extremes; forbear resenting injuries so much
                as you think they deserve.
              </li>
              <li className="">
                Cleanliness. Tolerate no uncleanliness in body, cloaths, or
                habitation.
              </li>
              <li className="">
                Tranquillity. Be not disturbed at trifles, or at accidents
                common or unavoidable.
              </li>
              <li className="">
                Chastity. Rarely use venery but for health or offspring, never
                to dullness, weakness, or the injury of your own or another's
                peace or reputation.
              </li>
              <li className="">Humility. Imitate Jesus and Socrates.</li>
            </ol>

            <div className="mt-16">
              <a
                href="https://www.amazon.com/gp/product/0743255062/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Benjamin Franklin
              </a>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default FranklinPage;
