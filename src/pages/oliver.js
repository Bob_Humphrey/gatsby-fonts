import React, { useContext } from "react";
import Layout from "../components/layout";
import { GlobalStateContext } from "../context/GlobalContextProvider";

function OliverPage() {
  const state = useContext(GlobalStateContext);
  return (
    <Layout>
      <div className={state.backgroundColor + " flex justify-center"}>
        <div
          className={state.backgroundWidth + "  mx-auto text-justify py-12 "}
        >
          <div
            className={
              state.headingFont +
              " " +
              state.headingSize +
              " " +
              state.headingLineHeight +
              " " +
              state.headingColor +
              " mb-6"
            }
          >
            Angels
            <div
              className={
                state.bodyFont +
                " " +
                state.bodySize +
                " " +
                state.bodyLineHeight +
                " " +
                state.bodyColor +
                "  mx-auto text-justify py-6 "
              }
            >
              <div className="">You might see an angel anytime</div>
              <div className="">and anywhere. Of course you have</div>
              <div className="">to open your eyes to a kind of</div>
              <div className="">second level, but it’s not really</div>
              <div className="">hard. The whole business of</div>
              <div className="">what’s reality and what isn’t has</div>
              <div className="">never been solved and probably</div>
              <div className="">never will be. So I don’t care to</div>
              <div className="">be too definite about anything.</div>
              <div className="">I have a lot of edges called Perhaps</div>
              <div className="">and almost nothing you can call</div>
              <div className="">Certainty. For myself, but not</div>
              <div className="">for other people. That’s a place</div>
              <div className="">you just can’t get into, not</div>
              <div className="">entirely anyway, other people’s</div>
              <div className="mb-8">heads.</div>
              <div className="">I’ll just leave you with this.</div>
              <div className="">I don’t care how many angels can</div>
              <div className="">dance on the head of a pin. It’s</div>
              <div className="">enough to know that for some people</div>
              <div className="">they exist, and that they dance.</div>

              <div className="mt-16">
                <a
                  href="https://www.amazon.com/Blue-Horses-Poems-Mary-Oliver/dp/0143127810/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Mary Oliver
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default OliverPage;
