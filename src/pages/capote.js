import React, { useContext } from "react";
import Layout from "../components/layout";
import { GlobalStateContext } from "../context/GlobalContextProvider";

function CapotePage() {
  const state = useContext(GlobalStateContext);
  return (
    <Layout>
      <div className={state.backgroundColor + " flex justify-center"}>
        <div
          className={state.backgroundWidth + "  mx-auto text-justify py-12 "}
        >
          <div
            className={
              state.headingFont +
              " " +
              state.headingSize +
              " " +
              state.headingLineHeight +
              " " +
              state.headingColor +
              " mb-6"
            }
          >
            Breakfast at Tiffany's
          </div>
          <div
            className={
              state.bodyFont +
              " " +
              state.bodySize +
              " " +
              state.bodyLineHeight +
              " " +
              state.bodyColor +
              "  mx-auto text-justify py-6 "
            }
          >
            <p className="">
              I am always drawn back to places where I have lived, the houses
              and their neighborhoods. For instance, there is a brownstone in
              the East Seventies where, during the early years of the war, I had
              my first New York apartment. It was one room crowded with attic
              furniture, a sofa and fat chairs upholstered in that itchy,
              particular red velvet that one associates with hot days on a
              train. The walls were stucco, and a color rather like
              tobacco-spit. Everywhere, in the bathroom too, there were prints
              of Roman ruins freckled brown with age. The single window looked
              out on a fire escape. Even so, my spirits heightened whenever I
              felt in my pocket the key to this apartment; with all its gloom,
              it still was a place of my own, the first, and my books were
              there, and jars of pencils to sharpen, everything I needed, so I
              felt, to become the writer I wanted to be.
            </p>
            <p className="">
              It never occurred to me in those days to write about Holly
              Golightly, and probably it would not now except for a conversation
              I had with Joe Bell that set the whole memory of her in motion
              again.
            </p>
            <p className="">
              Holly Golightly had been a tenant in the old brownstone; she
              occupied the apartment below mine. As for Joe Bell, he ran a bar
              around the corner on Lexington Avenue; he still does. Both Holly
              and I used to go there six, seven times a day, not for a drink,
              not always, but to make telephone calls: during the war a private
              telephone was hard to come by. Moreover, Joe Bell was good about
              taking messages, which in Holly's case was no small favor, for she
              had a tremendous many.
            </p>
            <div className="mt-16">
              <a
                href="https://www.amazon.com/Breakfast-at-Tiffanys-Vintage-International-ebook/dp/B007OLYQ4E/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Truman Capote
              </a>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default CapotePage;
