import React, { useContext } from "react";
import Layout from "../components/layout";
import { GlobalStateContext } from "../context/GlobalContextProvider";

function WrightPage() {
  const state = useContext(GlobalStateContext);

  return (
    <Layout>
      <div className={state.backgroundColor + " flex justify-center"}>
        <div
          className={state.backgroundWidth + "  mx-auto text-justify py-12 "}
        >
          <div
            className={
              state.headingFont +
              " " +
              state.headingSize +
              " " +
              state.headingLineHeight +
              " " +
              state.headingColor +
              " mb-6"
            }
          >
            Black Boy
          </div>
          <div
            className={
              state.bodyFont +
              " " +
              state.bodySize +
              " " +
              state.bodyLineHeight +
              " " +
              state.bodyColor +
              "  mx-auto text-justify py-6 "
            }
          >
            <p className="">
              Once, in the night, my mother called me to her bed and told me
              that she could not endure the pain, that she wanted to die. I held
              her hand and begged her to be quiet. That night I ceased to react
              to my mother; my feelings were frozen. I merely waited upon her,
              knowing that she was suffering. She remained abed ten years,
              gradually growing better, but never completely recovering,
              relapsing periodically into her paralytic state. The family had
              stripped itself of money to fight my mother’s illness and there
              was no more forthcoming. Her illness gradually became an accepted
              thing in the house, something that could not be stopped or helped.
              My mother’s suffering grew into a symbol in my mind, gathering to
              itself all the poverty, the ignorance, the helplessness; the
              painful, baffling, hunger-ridden days and hours; the restless
              moving, the futile seeking, the uncertainty, the fear, the dread;
              the meaningless pain and the endless suffering. Her life set the
              emotional tone of my life, colored the men and women I was to meet
              in the future, conditioned my relation to events that had not yet
              happened, determined my attitude to situations and circumstances I
              had yet to face. A somberness of spirit that I was never to lose
              settled over me during the slow years of my mother’s unrelieved
              suffering, a somberness that was to make me stand apart and look
              upon excessive joy with suspicion, that was to make me
              self-conscious, that was to make me keep forever on the move, as
              though to escape a nameless fate seeking to overtake me. At the
              age of twelve, before I had had one full year of formal schooling,
              I had a conception of life that no experience would ever erase, a
              predilection for what was real that no argument could ever
              gainsay, a sense of the world that was mine and mine alone, a
              notion as to what life meant that no education could ever alter, a
              conviction that the meaning of living came only when one was
              struggling to wring a meaning out of meaningless suffering.
            </p>

            <div className="mt-16">
              <a
                href="https://www.amazon.com/Black-Seventy-fifth-Anniversary-Richard-Wright/dp/0062964135/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Richard Wright
              </a>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default WrightPage;
