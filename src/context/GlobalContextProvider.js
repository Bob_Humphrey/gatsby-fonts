import React from "react";

export const GlobalStateContext = React.createContext();
export const GlobalDispatchContext = React.createContext();

const initialState = {
  category: `Background`,
  subCategory: `Color`,
  backgroundSubCategory: `Color`,
  headingSubCategory: `Font`,
  bodySubCategory: `Font`,
  backgroundWidth: `w-1/2`,
  backgroundColor: `bg-white`,
  headingFont: `font-chunk`,
  headingFontFamily: `Chunk`,
  headingSize: `text-5xl`,
  headingLineHeight: `leading-tight`,
  headingColor: `text-black`,
  bodyFont: `font-inter_regular`,
  bodyFontFamily: `Inter`,
  bodySize: `text-lg`,
  bodyLineHeight: `leading-normal`,
  bodyColor: `text-black`
};

function reducer(state, action) {
  switch (action.type) {
    case `CHANGE_CATEGORY`: {
      let updatedSubCategory;
      if (action.property === "Background") {
        updatedSubCategory = state.backgroundSubCategory;
      } else if (action.property === "Heading") {
        updatedSubCategory = state.headingSubCategory;
      } else if (action.property === "Body") {
        updatedSubCategory = state.bodySubCategory;
      }
      return {
        ...state,
        category: action.property,
        subCategory: updatedSubCategory
      };
    }
    case `CHANGE_SUBCATEGORY`: {
      switch (state.category) {
        case `Background`:
          return {
            ...state,
            backgroundSubCategory: action.property,
            subCategory: action.property
          };
        case `Heading`:
          return {
            ...state,
            headingSubCategory: action.property,
            subCategory: action.property
          };
        case `Body`:
          return {
            ...state,
            bodySubCategory: action.property,
            subCategory: action.property
          };
        default:
          return null;
      }
    }
    case `CHANGE_BACKGROUND_WIDTH`: {
      return {
        ...state,
        backgroundWidth: action.property
      };
    }
    case `CHANGE_BACKGROUND_COLOR`: {
      return {
        ...state,
        backgroundColor: action.property
      };
    }
    case `CHANGE_HEADING_FONT`: {
      if (state.subCategory === "Font") {
        return {
          ...state,
          headingFont: action.property,
          headingFontFamily: action.family,
          headingFontFamilyMainFont: action.property
        };
      } else {
        return {
          ...state,
          headingFont: action.property
        };
      }
    }
    case `CHANGE_HEADING_SIZE`: {
      return {
        ...state,
        headingSize: action.property
      };
    }
    case `CHANGE_HEADING_LINEHEIGHT`: {
      return {
        ...state,
        headingLineHeight: action.property
      };
    }
    case `CHANGE_HEADING_COLOR`: {
      return {
        ...state,
        headingColor: action.property
      };
    }
    case `CHANGE_BODY_FONT`: {
      if (state.subCategory === "Font") {
        return {
          ...state,
          bodyFont: action.property,
          bodyFontFamily: action.family,
          bodyFontFamilyMainFont: action.property
        };
      } else {
        return {
          ...state,
          bodyFont: action.property
        };
      }
    }
    case `CHANGE_BODY_SIZE`: {
      return {
        ...state,
        bodySize: action.property
      };
    }
    case `CHANGE_BODY_LINEHEIGHT`: {
      return {
        ...state,
        bodyLineHeight: action.property
      };
    }
    case `CHANGE_BODY_COLOR`: {
      return {
        ...state,
        bodyColor: action.property
      };
    }
    default:
      throw new Error(`Bad Action Type`);
  }
}

const GlobalContextProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  return (
    <GlobalStateContext.Provider value={state}>
      <GlobalDispatchContext.Provider value={dispatch}>
        {children}
      </GlobalDispatchContext.Provider>
    </GlobalStateContext.Provider>
  );
};

export default GlobalContextProvider;
